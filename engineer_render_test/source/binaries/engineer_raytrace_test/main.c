#include "main.h"

static const Ecore_Getopt optdesc = {
  "engineer_render_test", "%prog [options]",
  PACKAGE_VERSION,
  COPYRIGHT,
  "3 clause BSD license",
  "An Enlightenment Foundation Library based program.",
  0,
  {
    ECORE_GETOPT_LICENSE('L', "license"),
    ECORE_GETOPT_COPYRIGHT('C', "copyright"),
    ECORE_GETOPT_VERSION('V', "version"),
    ECORE_GETOPT_HELP('h', "help"),
    ECORE_GETOPT_SENTINEL
  }
};

void
elm_opts(int argc, char **argv)
{
   int       args;
   Eina_Bool quit_option = EINA_FALSE;

   Ecore_Getopt_Value values[] = {
     ECORE_GETOPT_VALUE_BOOL(quit_option),
     ECORE_GETOPT_VALUE_BOOL(quit_option),
     ECORE_GETOPT_VALUE_BOOL(quit_option),
     ECORE_GETOPT_VALUE_BOOL(quit_option),
     ECORE_GETOPT_VALUE_NONE
   };

   #if ENABLE_NLS
   setlocale(LC_ALL, "");
   bindtextdomain(PACKAGE, LOCALEDIR);
   bind_textdomain_codeset(PACKAGE, "UTF-8");
   textdomain(PACKAGE);
   #endif

   args = ecore_getopt_parse(&optdesc, values, argc, argv);
   if (args < 0)
   {
      EINA_LOG_CRIT("Could not parse arguments.");
      //goto engineer_raytrace_test_error;
   }
   else if (quit_option)
   {
      //goto engineer_raytrace_test_error;
   }

   printf("Name: %s, Version: %s\n", argv[0], PACKAGE_VERSION);
}

EAPI_MAIN int
elm_main(int argc, char **argv)
{
   Eo *window, *viewport;

   // Parse and handle any command line inputs and then set our Elementary application info.
   elm_opts(argc, argv);
   elm_app_info_set(elm_main, "engineer_raytrace_test", "images/engineer_raytrace_test.png");

   // Initialize the Engineer library and set our host unique game Node properties.
   eng_init();

   eng_node_game_set(eng_node(), "Engineer Raytracer Test");
   eng_node_path_set(eng_node(), "..");

   window   = engineer_raytrace_test_window_init();
   viewport = engineer_raytrace_test_viewport_init(window);

   engineer_raytrace_test_scene_init(viewport);

   //eng_run();
   elm_run();

   //elm_shutdown();
   //eng_shutdown();

   //config_error:
      //engineer_raytrace_test_shutdown();

   //engineer_raytrace_test_error:
      //engineer_raytrace_test_shutdown();

   return 0;
}
ELM_MAIN()

Eo *
engineer_raytrace_test_window_init()
{
   Evas_Object *window;

   //efl_domain_current_push(EFL_ID_DOMAIN_SHARED);
   window = elm_win_util_standard_add("main", "Engineer Raytrace Test");
   //efl_domain_current_pop();
   elm_win_focus_highlight_enabled_set(window, EINA_TRUE);
   elm_policy_set(ELM_POLICY_QUIT, ELM_POLICY_QUIT_LAST_WINDOW_CLOSED);
   elm_win_autodel_set(window, EINA_TRUE);
   //evas_object_smart_callback_add(window, "delete,request", _eng_window_del, window);
   evas_object_resize(window, 800, 800);
   evas_object_show(window);

   return window;
}

Eo *
engineer_raytrace_test_scene_init(Eo *viewport)
{
   Eo                     *scene, *frame;
   Core_Transform_State    transform;
   Core_Camera_State       camera;
   Core_FPS_Control_State  control;
   Input_Keyboard_State    keyboard;
   Input_Mouse_State       mouse;
   Voxel_Model_State       model;

   EntityID camera1, sphere1, sphere2, box1, plane1 EINA_UNUSED;


   // In order to recieve hardware device inputs from GUI elements, we need to associate some
   //    actions with some Entity Events in an input context.
   eng_node_input_context_new(eng_node(), "noclip");

   eng_node_input_context_assign(eng_node(), "noclip", "w",           "move_forward");
   eng_node_input_context_assign(eng_node(), "noclip", "a",           "move_left");
   eng_node_input_context_assign(eng_node(), "noclip", "s",           "move_rearward");
   eng_node_input_context_assign(eng_node(), "noclip", "d",           "move_right");
   eng_node_input_context_assign(eng_node(), "noclip", "q",           "roll_left");
   eng_node_input_context_assign(eng_node(), "noclip", "e",           "roll_right");
   eng_node_input_context_assign(eng_node(), "noclip", "mouse_right", "yaw_left");
   eng_node_input_context_assign(eng_node(), "noclip", "mouse_left",  "yaw_right");
   eng_node_input_context_assign(eng_node(), "noclip", "mouse_up",    "pitch_rearward");
   eng_node_input_context_assign(eng_node(), "noclip", "mouse_down",  "pitch_forward");


   // Create a Scene and get a viewport frame to output our GFX to.
   scene = eng_scene_new(NULL);
   frame = eng_viewport_frame_get(viewport);


   // In order for a Scene to do anything, we need to load some Assets!
   eng_scene_asset_load(scene, "Core");
   eng_scene_asset_load(scene, "Input");
   eng_scene_asset_load(scene, "Voxel");


   // Lets make a Camera!
   transform.position    = vec3(0.0, 0.0, 16.0);
   transform.orientation = quat(0.0, 0.0,  1.0, 0.0);

   keyboard.context = (Pntr)"noclip";
   keyboard.origin  = (Pntr)frame;

   mouse.context = (Pntr)"noclip";
   mouse.origin  = (Pntr)frame;

   camera1 = eng_scene_entity_create(scene, NULL_ID);
   eng_scene_component_attach(scene, camera1, "Core.Transform.Component",   (Data*)&transform);
   eng_scene_component_attach(scene, camera1, "Core.Camera.Component",      (Data*)&camera);
   eng_scene_component_attach(scene, camera1, "Core.FPS_Control.Component", (Data*)&control);
   eng_scene_component_attach(scene, camera1, "Input.Keyboard.Component",   (Data*)&keyboard);
   eng_scene_component_attach(scene, camera1, "Input.Mouse.Component",      (Data*)&mouse);
   eng_viewport_look(viewport, camera1);


   // We also need some Entities to look at.
   transform.position    = vec3(4.0, 0.0, 0.0);
   transform.orientation = quat(0.0, 1.0, 0.0, 0.0);

   model.shape  = 1;
   model.bounds = vec3(1.0, 1.0, 1.0);
   model.color  = color(0.0, 1.0, 1.0, 0.0);

   sphere1 = eng_scene_entity_create(scene, NULL_ID);
   eng_scene_component_attach(scene, sphere1, "Core.Transform.Component", (Data*)&transform);
   eng_scene_component_attach(scene, sphere1, "Voxel.Model.Component",    (Data*)&model);


   transform.position    = vec3(0.0, 4.0, 0.0);
   transform.orientation = quat(0.0, 1.0, 0.0, 0.0);

   model.shape  = 1;
   model.bounds = vec3(1.0, 1.0, 1.0);
   model.color  = color(0.0, 0.0, 1.0, 0.0);

   sphere2 = eng_scene_entity_create(scene, NULL_ID);
   eng_scene_component_attach(scene, sphere2, "Core.Transform.Component", (Data*)&transform);
   eng_scene_component_attach(scene, sphere2, "Voxel.Model.Component",    (Data*)&model);


   transform.position    = vec3(0.0, 0.0, 0.0);
   transform.orientation = quat(0.0, 0.0, 1.0, (3.14159 * 0.25));

   model.shape  = 2;
   model.bounds = vec3(1.0, 1.0, 1.0);
   model.color  = color(0.0, 1.0, 0.0, 0.0);

   box1 = eng_scene_entity_create(scene, NULL_ID);
   eng_scene_component_attach(scene, box1, "Core.Transform.Component", (Data*)&transform);
   eng_scene_component_attach(scene, box1, "Voxel.Model.Component",    (Data*)&model);
/*
   transform.position    = vec3(0.0, -4.0, 0.0);
   transform.orientation = quat(0.0,  1.0, 0.0, 0.0);

   model.shape  = 3;
   model.bounds = vec3(1.0, 1.0, 1.0);
   model.color  = color(1.0, 0.8, 0.6, 0.0);

   plane1 = eng_scene_entity_create(scene, NULL_ID);
   eng_scene_component_attach(scene, plane1, "Core.Transform.Component", (Data*)&transform);
   eng_scene_component_attach(scene, plane1, "Voxel.Model.Component",    (Data*)&model);
*/
   return scene;
}

Eo *
engineer_raytrace_test_viewport_init(Eo *window)
{
   Eo *viewport = eng_viewport_add(window);
   //elm_win_resize_object_add(window, viewport);

   //evas_object_size_hint_weight_set(viewport, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
   //evas_object_size_hint_align_set(viewport, EVAS_HINT_FILL, EVAS_HINT_FILL);

   //evas_object_show(viewport);

   return viewport;
}

