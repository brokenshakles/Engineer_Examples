#include "main.h"

#include "opts.c"
#include "mesh.c"


// Lets make a Camera!
static Entity
_camera_factory(Entity scene, Vec3 position, Eo *viewport)
{
   Entity camera = eng_entity_create(scene);

   Core_Transform_State   transform;
   Core_Camera_State      camera;
   Core_FPS_Control_State control;
   Input_Keyboard_State   keyboard;
   Input_Mouse_State      mouse;

   transform.position    = position;
   transform.orientation = quat(0.0,  0.0,  0.0, -1.0);

   camera.projection    = "perspective";
   camera.aperture_size = 1;
   camera.field_of_view = 115;

   keyboard.context = "noclip";
   keyboard.origin  = viewport; // We have taken eng_viewport_frame_get(viewport) out of here and
                                //    will be moving it into Input.Keyboard and Input.Mouse
   mouse.context = "noclip";
   mouse.origin  = viewport; // Same as above.

   eng_component_attach(camera, "Core.Transform",   &transform);
   eng_component_attach(camera, "Core.Camera",      &camera);
   eng_component_attach(camera, "Core.FPS_Control", &control);
   eng_component_attach(camera, "Input.Keyboard",   &keyboard);
   eng_component_attach(camera, "Input.Mouse",      &mouse);

   return camera;
}

static Entity
_cube_factory(Entity scene, Vec3 postion)
{
   Entity cube = eng_entity_create(scene);

   transform.position    = position;
   transform.orientation = quat(0.0,  0.0,  1.0,  0.0);

   model.bounds = vec3(0.0,  0.0,  0.0);
   model.scale  = vec3(1.0,  1.0,  1.0);
   model.mesh   = eng_cube_mesh_init();

   eng_component_attach(cube, "Core.Transform", &transform);
   eng_component_attach(cube, "Polygon.Model",  &model);

   return cube;
}

static Entity
_scene_factory()
{
   Entity scene = eng_scene_create();

   // In order for a Scene to do anything, we need to load some Assets!
   eng_scene_asset_load(scene, "Core");
   eng_scene_asset_load(scene, "Input");
   eng_scene_asset_load(scene, "Polygon");

   return scene;
}

// In order to recieve hardware device inputs from GUI elements, we need to associate some
//    actions with some Entity Events in an input context.
static void
_input_context_init(String *symbol)
{
   eng_input_context_new(symbol);

   eng_input_context_assign(symbol, "w",           "move_forward");
   eng_input_context_assign(symbol, "a",           "move_left");
   eng_input_context_assign(symbol, "s",           "move_rearward");
   eng_input_context_assign(symbol, "d",           "move_right");
   eng_input_context_assign(symbol, "q",           "roll_left");
   eng_input_context_assign(symbol, "e",           "roll_right");
   eng_input_context_assign(symbol, "mouse_right", "yaw_left");
   eng_input_context_assign(symbol, "mouse_left",  "yaw_right");
   eng_input_context_assign(symbol, "mouse_up",    "pitch_rearward");
   eng_input_context_assign(symbol, "mouse_down",  "pitch_forward");
}

EAPI_MAIN int
efl_main(void *data EINA_UNUSED, const Efl_Event *event)
{
   Eo    *node,   *window, *viewport;
   Entity scene_0, cube_0,  cube_1,  camera_0;

   // Initialize the Engineer library and set our host unique game Node properties.
   node = eng_init(event, efl_opts);
   eng_node_game_set(node, "Engineer Rasterizer Test");
   eng_node_path_set(node, "..");

   // We will need a Window with an embedded Viewport to display our graphical output.
   window = efl_add(EFL_UI_WIN_CLASS, event->object,
               efl_ui_win_type_set(efl_added, EFL_UI_WIN_TYPE_BASIC),
               efl_text_set(efl_added, eng_node_game_get()),
               efl_ui_win_autodel_set(efl_added, EINA_TRUE)
   );

   viewport = efl_add(ENG_VIEWPORT_CLASS, window,
                 efl_content_set(window, efl_added),
                 efl_gfx_hint_size_min_set(efl_added, EINA_SIZE2D(800, 800),
                 efl_gfx_hint_weight_set(efl_added, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND),
                 efl_gfx_hint_align_set(efl_added, EVAS_HINT_FILL, EVAS_HINT_FILL)
   );

   // An input context is needed to capture Keyboard and Mouse input and convert them to Eng_Events;
   _input_context_init("noclip")

   // Now to make a Scene and put some objects in it to render!
   scene_0  =  _scene_factory();
   cube_0   =   _cube_factory(scene_0, vec3(0.0,  0.0,  0.0));
   cube_1   =   _cube_factory(scene_0, vec3(0.0,  2.0,  0.0));
   camera_0 = _camera_factory(scene_0, vec3(0.0,  0.0,  8.0), viewport);

   // This connects our camera_0 Entity to our viewport EFL widget.
   eng_viewport_look(viewport, camera_0);

   eng_shutdown();
   efl_exit(0);
}
EFL_MAIN()

