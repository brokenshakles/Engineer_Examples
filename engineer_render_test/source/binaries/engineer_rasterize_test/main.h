#ifndef _ENGINEER_RASTERIZE_TEST_MAIN_H_
#define _ENGINEER_RASTERIZE_TEST_MAIN_H_

#define COPYRIGHT "Copyright © 2016 Matthew Kolar <mjkolar@charter.net> and various contributors \
                     (see AUTHORS)."

#include <stdio.h>
#include <engineer.h>

#include "Core/Transform/Component.h"
#include "Core/Camera/Component.h"
#include "Core/FPS_Control/Component.h"
#include "Input/Mouse/Component.h"
#include "Input/Keyboard/Component.h"
#include "Polygon/Model/Component.h"

static Eo *
engineer_rasterize_test_window_init();

static Eo *
engineer_rasterize_test_viewport_init(Eo *window);

static Eo *
engineer_rasterize_test_scene_init(Eo *viewport);

#endif

//#ifdef HAVE_CONFIG_H
//# include "config.h"
//#endif

//#if ENABLE_NLS
//# include <libintl.h>
//#endif

