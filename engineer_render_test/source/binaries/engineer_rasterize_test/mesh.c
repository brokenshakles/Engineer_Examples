#include "mesh.h"

static const float
cube_vertices[] =
{
   // New Order
   -0.5f,  0.5f,  0.5f, // 0
   -0.5f, -0.5f,  0.5f, // 1
    0.5f,  0.5f,  0.5f, // 2
    0.5f, -0.5f,  0.5f, // 3
    0.5f,  0.5f, -0.5f, // 4
    0.5f, -0.5f, -0.5f, // 5
   -0.5f,  0.5f, -0.5f, // 6
   -0.5f, -0.5f, -0.5f, // 7
};

static const unsigned int
cube_elements[] =
{
   // Each line is two tris.
   0, 1, 2, 2, 1, 3, // Front
   2, 3, 4, 4, 3, 5, // Right
   4, 5, 6, 6, 5, 7, // Back
   6, 7, 0, 0, 7, 1, // Left
   6, 0, 4, 4, 0, 2, // Top
   1, 7, 3, 3, 7, 5  // Bottom
};

static const float
cube_colors[] =
{
   0.0625f,     0.57421875f, 0.92578125f, 1.0f, // 0
   0.0625f,     0.57421875f, 0.92578125f, 1.0f, // 1
   0.0625f,     0.57421875f, 0.92578125f, 1.0f, // 2
   0.0625f,     0.57421875f, 0.92578125f, 1.0f, // 3
   0.52734375f, 0.76171875f, 0.92578125f, 1.0f, // 4
   0.52734375f, 0.76171875f, 0.92578125f, 1.0f, // 5
   0.52734375f, 0.76171875f, 0.92578125f, 1.0f, // 6
   0.52734375f, 0.76171875f, 0.92578125f, 1.0f, // 7
};

Eng_Mesh *
eng_cube_mesh_init()
{
   Eng_Mesh *cube = malloc(sizeof(Eng_Mesh));

   cube->orientation = eng_math_mtrx_identity();

   cube->vertices = eina_inarray_new(sizeof(float) * 3,    0);
   cube->elements = eina_inarray_new(sizeof(unsigned int), 0);
   cube->textures = eina_inarray_new(sizeof(float) * 4,    0);

   cube->children = NULL;

   //cube->faces    = NULL;
   //cube->normals  = NULL;
   //cube->material = NULL;

   for(int i = 0; i < 24; i+=3) eina_inarray_push(cube->vertices, &cube_vertices[i]);
   for(int i = 0; i < 36;  ++i) eina_inarray_push(cube->elements, &cube_elements[i]);
   for(int i = 0; i < 32; i+=4) eina_inarray_push(cube->textures, &cube_colors[i]);

   return cube;
}
