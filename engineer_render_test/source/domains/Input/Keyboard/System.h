#ifndef _INPUT_KEYBOARD_SYSTEM_H_
#define _INPUT_KEYBOARD_SYSTEM_H_

#include <eng_system.h>

ESML(
   SYMBOL(Input.Keyboard)
   SYSTEM(
      PREREQ()
      SCENEX()
      EVENTS()
   )
   ENTITY(
      IGNORE()
      TARGET(Input.Keyboard)
      IMPORT(
         ASSET(Input,
            COMPONENT(Keyboard,
               FIELD(Pntr, context)
               FIELD(Pntr, origin)
      )  )  )
      EXPORT(
         ASSET(Input,
            COMPONENT(Keyboard,
               FIELD(Pntr, context)
               FIELD(Pntr, origin)
      )  )  )
      EVENTS(
         EVENT(input_context_set,   sizeof(Pntr))
         EVENT(keyboard_origin_set, sizeof(Pntr))
)  )  )

#endif

