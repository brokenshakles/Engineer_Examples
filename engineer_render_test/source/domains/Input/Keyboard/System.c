#include "System.h"

static void
_eng_input_keyboard_key_down_cb(void *data, Evas *e, Evas_Object *obj, void *event_info);

static void
_eng_input_keyboard_key_down_origin_update(EntityID entity, Eo *origin, Eina_Hash *context);


static void
system_start(System_State *system)
{
   printf("System Start Checkpoint. System: Input.Keyboard.\n");
}

static void
system_update(System_State *system)
{
}

static void
system_stop(System_State *system)
{
}

static void
entity_start(Entity_State *entity)
{
   String *context_name = (String*)entity->Input.Keyboard.context;
   Eo     *origin       = (Eo*)entity->Input.Keyboard.origin;

   entity->Input.Keyboard.context = NULL;
   entity->Input.Keyboard.origin  = NULL;

   entity_invoke(input_context_set,   context_name);
   entity_invoke(keyboard_origin_set, origin);
}

static void
entity_update(Entity_State *entity)
{
}

static void
entity_stop(Entity_State *entity)
{
   entity_invoke(keyboard_origin_set, NULL);
}

static bool
entity_event__input_context_set(Entity_State *entity, const void *notice)
{
   const String *context_name = notice;
   Eina_Hash    *context = eina_hash_find(eng_node_input_contexts_get(eng_node()), context_name);

   if(!context) return EINA_FALSE;

   entity->Input.Keyboard.context = (Data*)context_name;

   if(entity->Input.Keyboard.origin)
      _eng_input_keyboard_key_down_origin_update(this, (Eo*)entity->Input.Keyboard.origin, context);

   return EINA_TRUE;
}

static bool
entity_event__keyboard_origin_set(Entity_State *entity, const void *notice)
{
   const Eo  *origin       = notice;
   String    *context_name = (String*)entity->Input.Keyboard.context;
   Eina_Hash *context      = eina_hash_find(eng_node_input_contexts_get(eng_node()), context_name);

   // If data is not NULL, make sure it is a valid Eo.
   if(origin && !efl_alive_get(origin)) return EINA_FALSE;

   entity->Input.Keyboard.origin = (Pntr)origin;

   _eng_input_keyboard_key_down_origin_update(this, (Eo*)origin, context);

   return EINA_TRUE;
}

static void
_eng_input_keyboard_key_down_cb(void *data,
        Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info)
{
   Evas_Event_Key_Down *event   = event_info;
   Eina_Hash           *targets = data;
   Eng_Node_Input       input;

   input.symbol  = event->keyname;
   input.payload = NULL;

   eina_hash_foreach(targets, eng_node_input_context_entity_notify_from_efl, &input);
}

static void
_eng_input_keyboard_key_down_origin_update(EntityID entity, Eo *origin, Eina_Hash *context)
{
   Eng_Node_Input_Callback *callback;

   callback          = malloc(sizeof(Eng_Node_Input_Callback));
   callback->entity  = entity;
   callback->origin  = origin;
   callback->context = context;

   callback->efl_key_name  = "Eng_Input_Keyboard_Key_Down_Listeners";
   callback->efl_cb_func   = _eng_input_keyboard_key_down_cb;
   callback->efl_cb_symbol = EVAS_CALLBACK_KEY_DOWN;

   ecore_main_loop_thread_safe_call_async(eng_node_input_context_origin_update_cb, callback);
}

