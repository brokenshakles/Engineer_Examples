#include "System.h"

static void
_eng_input_mouse_pointer_move_cb(void *data, Evas *e, Evas_Object *obj, void *event_info);

static void
_eng_input_mouse_pointer_move_origin_update(EntityID entity, Eo *origin, Eina_Hash *context);


void
system_start(System_State *system)
{
   printf("System Start Checkpoint. System: Input.Mouse.\n");
}

void
system_update(System_State *system)
{
}

void
system_stop(System_State *system)
{
}

void
entity_start(Entity_State *entity)
{
   const String *context_name = (String*)entity->Input.Mouse.context;
   const Eo     *origin       = (Eo*    )entity->Input.Mouse.origin;

   entity->Input.Mouse.context = NULL;
   entity->Input.Mouse.origin  = NULL;

   entity_invoke(input_context_set, context_name);
   entity_invoke(mouse_origin_set,  origin);
}

void
entity_update(Entity_State *entity)
{
}

void
entity_stop(Entity_State *entity)
{
   entity_invoke(mouse_origin_set, NULL);
}

static bool
entity_event__input_context_set(Entity_State *entity, const void *notice)
{
   const String *context_name = notice;
   Eina_Hash    *context      = eina_hash_find(eng_node_input_contexts_get(eng_node()), context_name);

   if(!context) return EINA_FALSE;

   entity->Input.Mouse.context = (Data*)context_name;

   if(entity->Input.Mouse.origin)
      _eng_input_mouse_pointer_move_origin_update(this, (Eo*)entity->Input.Mouse.origin, context);

   return EINA_TRUE;
}

static bool
entity_event__mouse_origin_set(Entity_State *entity, const void *notice)
{
   const Eo  *origin       = notice;
   String    *context_name = (String*)entity->Input.Mouse.context;
   Eina_Hash *context      = eina_hash_find(eng_node_input_contexts_get(eng_node()), context_name);

   // If data is not NULL, make sure it is a valid Eo.
   if(origin && !efl_alive_get(origin)) return EINA_FALSE;

   entity->Input.Mouse.origin = (Pntr)origin;

   _eng_input_mouse_pointer_move_origin_update(this, (Eo*)origin, context);

   return EINA_TRUE;
}

static void
_eng_input_mouse_pointer_move_cb(void *data,
        Evas *e EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info)
{
   Evas_Event_Mouse_Move *event   = event_info;
   Eina_Hash             *targets = data;
   Sclr                   delta;
   Eng_Node_Input         input;

   delta = BASIS * (event->cur.output.x - event->prev.output.x);
   if(delta != 0)
   {
      if(delta > 0) input.symbol = "mouse_right";
      if(delta < 0) input.symbol = "mouse_left";

      input.payload = &delta;

      eina_hash_foreach(targets, eng_node_input_context_entity_notify_from_efl, &input);
   }

   delta = BASIS * (event->cur.output.y - event->prev.output.y);
   if(delta != 0)
   {
      if(delta > 0) input.symbol = "mouse_up";
      if(delta < 0) input.symbol = "mouse_down";

      input.payload = &delta;

      eina_hash_foreach(targets, eng_node_input_context_entity_notify_from_efl, &input);
   }
}

static void
_eng_input_mouse_pointer_move_origin_update(EntityID entity, Eo *origin, Eina_Hash *context)
{
   Eng_Node_Input_Callback *callback;

   callback          = malloc(sizeof(Eng_Node_Input_Callback));
   callback->entity  = entity;
   callback->origin  = (Eo*)origin;
   callback->context = context;

   callback->efl_key_name  = "Eng_Input_Mouse_Pointer_Move_Listeners";
   callback->efl_cb_func   = _eng_input_mouse_pointer_move_cb;
   callback->efl_cb_symbol = EVAS_CALLBACK_MOUSE_MOVE;

   ecore_main_loop_thread_safe_call_async(eng_node_input_context_origin_update_cb, callback);
}

