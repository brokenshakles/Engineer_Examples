#ifndef _INPUT_MOUSE_SYSTEM_H_
#define _INPUT_MOUSE_SYSTEM_H_

#include <eng_system.h>

ESML(
   SYMBOL(Input.Mouse)
   PREREQ()
   ENTITY(
      FIELDS(
         DOMAIN(Input,
            COMPONENT(Mouse,
               FIELD(Pntr, context, IMPORT + EXPORT)
               FIELD(Pntr, origin,  IMPORT + EXPORT)
      )  )  )
      EVENTS(
         EVENT(input_context_set, sizeof(Pntr))
         EVENT(mouse_origin_set,  sizeof(Pntr))
)  )  )

#endif

