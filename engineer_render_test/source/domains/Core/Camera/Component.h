#ifndef _CORE_CAMERA_COMPONENT_H_
#define _CORE_CAMERA_COMPONENT_H_

#include <eng_component.h>

ECML(
   SYMBOL(Core.Camera)
   ENTITY(
      FIELD(Pntr, name)
      FIELD(Pntr, glviews)
      
      FIELD(Pntr, projection)
      FIELD(Sclr, aperture_size)
      FIELD(Sclr, field_of_view)
)  )

#endif

