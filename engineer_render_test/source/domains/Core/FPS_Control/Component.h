#ifndef _CORE_FPS_CONTROL_COMPONENT_H_
#define _CORE_FPS_CONTROL_COMPONENT_H_

#include <eng_component.h>

ECML(
   SYMBOL(Core.FPS_Control)
   ENTITY(
      FIELD(Sclr, topspeed)
      FIELD(Sclr, acceleration)
)  )

#endif

