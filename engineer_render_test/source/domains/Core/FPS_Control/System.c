#include "System.h"

static void
system_start(System_State *system)
{
   printf("System Start Checkpoint. System: FPS_Control.\n");
}

static void
system_update(System_State *system)
{
}

static void
system_stop(System_State *system)
{
}

static void
entity_start(Entity_State *entity)
{
}

static void
entity_update(Entity_State *entity)
{
   Quat translation, orientation, rotation, rotation_inv;

   if(entity->event.torque.yaw || entity->event.torque.pitch || entity->event.torque.roll)
   {
      rotation     = eng_math_eulr_to_quat(entity->event.torque);
      rotation_inv = eng_math_quat_invert(rotation);

      orientation = entity->Core.Transform.orientation;
      orientation = eng_math_quat_multiply(rotation, orientation);
      orientation = eng_math_quat_normalize(orientation);

      entity->Core.Transform.orientation = orientation;
   }

   if(entity->event.translation.x || entity->event.translation.y || entity->event.translation.z)
   {
      rotation     = entity->Core.Transform.orientation;
      rotation_inv = eng_math_quat_invert(rotation);

      translation = eng_math_quat_pure(entity->event.translation);
      translation = eng_math_quat_multiply(rotation_inv, translation);
      translation = eng_math_quat_multiply(translation,  rotation);

      entity->Core.Transform.position.x += translation.x;
      entity->Core.Transform.position.y += translation.y;
      entity->Core.Transform.position.z += translation.z;
   }
}

static void 
entity_stop(Entity_State *entity)
{
}

static bool
entity_event__move_forward(Entity_State *entity, const void *notice)
{
   entity->event.translation.z = (Sclr)(-0.25 * BASIS); // W

   return EINA_TRUE;
}

static bool
entity_event__move_rearward(Entity_State *entity, const void *notice)
{
   entity->event.translation.z = (Sclr)( 0.25 * BASIS); // S

   return EINA_TRUE;
}

static bool
entity_event__move_left(Entity_State *entity, const void *notice)
{
   entity->event.translation.x = (Sclr)(-0.25 * BASIS); // A

   return EINA_TRUE;
}

static bool
entity_event__move_right(Entity_State *entity, const void *notice)
{
   entity->event.translation.x = (Sclr)( 0.25 * BASIS); // D

   return EINA_TRUE;
}

static bool
entity_event__roll_left(Entity_State *entity, const void *notice)
{
   entity->event.torque.roll = (1 << 10); // Q

   return EINA_TRUE;
}

static bool
entity_event__roll_right(Entity_State *entity, const void *notice)
{
   entity->event.torque.roll = -(1 << 10); // E

   return EINA_TRUE;
}

static bool
entity_event__yaw_left(Entity_State *entity, const void *notice)
{
   Sclr delta = *(const Sclr*)notice;

   entity->event.torque.yaw += delta / 128; // Camera movement to the left.

   return EINA_TRUE;
}

static bool
entity_event__yaw_right(Entity_State *entity, const void *notice)
{
   Sclr delta = *(const Sclr*)notice;

   entity->event.torque.yaw += delta / 128; // Camera movement to the right;

   return EINA_TRUE;
}

static bool
entity_event__pitch_forward(Entity_State *entity, const void *notice)
{
   Sclr delta = *(const Sclr*)notice;

   entity->event.torque.pitch += delta / 128; // Camera movement up.

   return EINA_TRUE;
}


static bool
entity_event__pitch_rearward(Entity_State *entity, const void *notice)
{
   Sclr delta = *(const Sclr*)notice;

   entity->event.torque.pitch += delta / 128; // Camera movement down.

   return EINA_TRUE;
}


