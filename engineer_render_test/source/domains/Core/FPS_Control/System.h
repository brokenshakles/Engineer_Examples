#ifndef _CORE_FPS_CONTROL_SYSTEM_H_
#define _CORE_FPS_CONTROL_SYSTEM_H_

#include <eng_system.h>

ESML(
   SYMBOL(Core.FPS_Control)
   SYSTEM(
      PREREQ()
      FIELDS()
      EVENTS()
   )
   ENTITY(
      IGNORE()
      TARGET(Core.Transform && Core.Collider)
      IMPORT(
         ASSET(Core,
            COMPONENT(FPS_Control,
               FIELD(Sclr, topspeed)
               FIELD(Sclr, acceleration)
            )
            COMPONENT(Transform,
               FIELD(Vec3, position)
               FIELD(Quat, orientation)
      )  )  )
      EXPORT(
         ASSET(Core,
            COMPONENT(Transform,
               FIELD(Vec3, position)
               FIELD(Quat, orientation)
      )  )  )
      EVENTS(
         FIELD(Vec3, translation)
         FIELD(Eulr, torque)

         EVENT(move_forward,   0)
         EVENT(move_rearward,  0)
         EVENT(move_left,      0)
         EVENT(move_right,     0)
         EVENT(roll_left,      0)
         EVENT(roll_right,     0)
         EVENT(yaw_left,       sizeof(Sclr))
         EVENT(yaw_right,      sizeof(Sclr))
         EVENT(pitch_forward,  sizeof(Sclr))
         EVENT(pitch_rearward, sizeof(Sclr))
)  )  )

#endif

