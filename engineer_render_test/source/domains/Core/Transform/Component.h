#ifndef _CORE_TRANSFORM_COMPONENT_H_
#define _CORE_TRANSFORM_COMPONENT_H_

#include <eng_component.h>

ECML(
   SYMBOL(Core.Transform)
   ENTITY(
      FIELD(Vec3, position)
      FIELD(Quat, orientation)

      FIELD(Sclr, poslock)
      FIELD(Sclr, orilock)
)  )

#endif


