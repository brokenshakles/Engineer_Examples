#version 310 es

#ifdef GL_ES
precision highp float;
#endif

in  lowp vec4 o_color;
out      vec4 Fragment_Color;


void main()
{
   Fragment_Color = o_color;
}
// Ok, that makes sense! the out v_color in polygon.vert connects to the in v_color in polygon.frag!
