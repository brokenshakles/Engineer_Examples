#version 310 es

#ifdef GL_ES
precision mediump float;
#endif

uniform mat4 u_mvp;
in      vec4 i_vertex;
in      vec4 i_color;
out     vec4 o_color;

void main()
{
   gl_Position = u_mvp * i_vertex;
   o_color     = i_color;
}
