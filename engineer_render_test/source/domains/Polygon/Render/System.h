#ifndef _POLYGON_RENDER_SYSTEM_H_
#define _POLYGON_RENDER_SYSTEM_H_

#include <eng_system.h>

ESML(
   SYMBOL(Polygon.Render)
   SYSTEM(
      PREREQ(
         ASSET(Polygon,
           SYSTEM(Broadphase)
      )  )
      SCENEX()
      EVENTS()
   )
   ENTITY(
      IGNORE()
      TARGET(Core.Transform && Core.Camera)
      IMPORT(
         ASSET(Core,
            COMPONENT(Transform,
               FIELD(Vec3, position)
               FIELD(Quat, orientation)
            )
            COMPONENT(Camera,
               FIELD(Pntr, glviews)
               FIELD(Pntr, projection)
               FIELD(Sclr, aperture_size)
               FIELD(Sclr, field_of_view)
      )  )  )
      EXPORT(
         ASSET(Core,
            COMPONENT(Camera,
               FIELD(Pntr, glviews)
               FIELD(Pntr, projection)
               FIELD(Sclr, aperture_size)
               FIELD(Sclr, field_of_view)
      )  )  )
      EVENTS(
         EVENT(viewport_attach, 8)
         EVENT(projection_set,  sizeof(Pntr))
)  )  )


typedef struct // appdata
{
   // GPU Program, Shader & Buffer Objects
   GLuint program;

   GLuint vertex_shader;
   GLuint color_shader;

   GLuint vertex_buffer;
   GLuint element_buffer;
   GLuint color_buffer;

   // These are storage location ID's for named fields inside of GLSL.
   GLuint mvp_gpuid;    // mvp_gpu_location
   GLuint vertex_gpuid; // vertex_gpu_location;
   GLuint color_gpuid;  // color_gpu_location;

   const System_State *system;
         Entity_State  camera;
}
Polygon_Render_Buffer;

#endif

