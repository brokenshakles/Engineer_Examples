#include "System.h"

struct _gl_data
{
   System_State *system;
   Entity_State *entity;
};

static void
_polygon_render_init_gl(Evas_Object *obj);

static void
_polygon_render_del_gl(Evas_Object *obj);

static void
_polygon_render_resize_gl(Evas_Object *obj);

static void
_polygon_render_draw_gl(Evas_Object *obj);
/*
static void
_polygon_render_object_add(Polygon_Render_Buffer *buffer, EntityID entity);

static void
_polygon_render_object_flush(Polygon_Render_Buffer *buffer);

static void
_polygon_render_frustum_sweep(Eo *obj, Engineer_Scene_Data *pd, Polygon_Render_Buffer *buffer);
*/

static void
system_start(System_State *system)
{
   printf("System Start Checkpoint. System: Polygon.Render.\n");
}

static void
system_update(System_State *system)
{
}

static void
system_stop(System_State *system)
{
}

static void
entity_start(Entity_State *entity)
{
   // Get the address of our Transform.
   entity->Core.Camera.glviews = (Pntr)eina_hash_pointer_new(NULL);

   if(!entity->Core.Camera.projection)    entity->Core.Camera.projection    = (Pntr)"perspective";
   if(!entity->Core.Camera.aperture_size) entity->Core.Camera.aperture_size = 1;
   if(!entity->Core.Camera.field_of_view) entity->Core.Camera.field_of_view = 90;
}

static Eina_Bool
_entity_update_glview(const Eina_Hash *hash EINA_UNUSED, const void *key EINA_UNUSED,
        void *data, void *fdata EINA_UNUSED)
{
   Elm_Glview      *glview = data;
   struct _gl_data *gldata = fdata;

   //Evas_GL_API           *api = elm_glview_gl_api_get(glview);
   Polygon_Render_Buffer *prb = efl_key_data_get(glview, "Polygon_Render_Buffer");
   if (prb)
   {
      prb->system =  gldata->system;

      prb->camera = *gldata->entity;

      elm_glview_changed_set(glview);
   }

   return EINA_TRUE;
}

// Our render system in this case is our frustum broadphase.
static void
entity_update(Entity_State *entity)
{
   struct _gl_data data;

   data.system = (System_State*)system;
   data.entity = entity;

   eina_hash_foreach((Eina_Hash*)entity->Core.Camera.glviews, _entity_update_glview, &data);
}

static void
entity_stop(Entity_State *entity)
{
}

static bool
entity_event__viewport_attach(Entity_State *entity, const void *notice)
{
   Eo                    *viewport = *(Eo**)notice;
   Elm_Glview            *glview   = ;
   Polygon_Render_Buffer *buffer   = calloc(1, sizeof(Polygon_Render_Buffer));

   efl_key_data_set(glview, "Polygon_Render_Buffer", buffer);

   eina_hash_set((Eina_Hash*)entity->Core.Camera.glviews, &viewport, glview);

   buffer->system =  system;
   buffer->camera = *entity;
}

static bool
entity_event__viewport_detach(Entity_State *entity, const void *notice)
{
}

static bool
entity_event__projection_set(Entity_State *entity, const void *notice)
{
   entity->Core.Camera.projection = (Pntr)*(String**)notice;

   return EINA_TRUE;
}

static Mtrx
_projection_mtrx_perspective(Sclr width, Sclr height, Sclr fov, Sclr near, Sclr far)
{
   Sclr left, right, bottom, top, aspect, scale, rlinv, tbinv, fninv, rladd, tbadd, fnadd;

   // This encodes a perspective projection.
   // Set up the projection's aspect-ratio and field-of-view (horizontal).
   scale = MULT(TAN(MULT(fov/2, DIVD(PI, sclr(180.0)) )), near);

   if (width > height)
   {
      aspect = DIVD(width, height); // The "normal" screen case.
      right  =  1 * MULT(scale, aspect); left   = -1 * MULT(scale, aspect);
      top    =  1 * scale;               bottom = -1 * scale;
   }
   else
   {
      aspect = DIVD(height, width); // The "tablet" screen case.
      right  =  1 * scale;               left   = -1 * scale;
      top    =  1 * MULT(scale, aspect); bottom = -1 * MULT(scale, aspect);
   }

   if(!(right - left) || !(top - bottom) || !(far - near))
      { Mtrx empty; memset(&empty, 0, sizeof(Mtrx)); return empty; }

   // Set up our perspective projection matrix.
   rlinv = DIVD(BASIS, right - left); rladd = right + left;
   tbinv = DIVD(BASIS, top - bottom); tbadd = top + bottom;
   fninv = DIVD(BASIS, far - near);   fnadd = far + near;

   Mtrx projection = {
      MULT(2 * near, rlinv),                     0,                                 0,      0,
                          0, MULT(2 * near, tbinv),                                 0,      0,
        -MULT(rladd, rlinv),   -MULT(tbadd, tbinv),               -MULT(fnadd, fninv), -BASIS,
                          0,                     0, -2 * MULT(MULT(far, near), fninv),      0
   };

   return projection;
}

static Mtrx
_projection_mtrx_orthographic(Sclr width, Sclr height, Sclr fov EINA_UNUSED, Sclr near, Sclr far)
{
   Sclr aspect, right, left, top, bottom, two, rlinv, tbinv, fninv;

   near = sclr(-1.0); far = sclr(100.0);

   if (width >= height)
   {
      aspect = DIVD(width, height); // The "normal" screen case.
      right =  aspect; left   = -1 * aspect;
      top   =  BASIS;  bottom = -1 * BASIS;
   }
   else
   {
      aspect = DIVD(height, width); // The "tablet" screen case.
      right =  BASIS; left   = -1 * BASIS;
      top   = aspect; bottom = -1 * aspect;
   }

   if(!(right - left) || !(top - bottom) || !(far - near))
      { Mtrx empty; memset(&empty, 0, sizeof(Mtrx)); return empty; }

   // Set up our orthographic projection matrix.
   two   = sclr(2.0);
   rlinv = DIVD(BASIS, right - left);
   tbinv = DIVD(BASIS, top - bottom);
   fninv = DIVD(BASIS, far - near);

   Mtrx projection = {
                MULT(two, rlinv),                          0,                        0,         0,
                               0,           MULT(two, tbinv),                        0,         0,
                               0,                          0,        -MULT(two, fninv),         0,
      -MULT(right + left, rlinv), -MULT(top + bottom, tbinv), -MULT(far + near, fninv),     BASIS
   };

   return projection;
}

static Mtrx
_view_mtrx(Entity_State *camera)
{
   Vec3 position;
   Quat orientation;

   position    = eng_math_vec3_invert(camera->Core.Transform.position);
   orientation = eng_math_quat_invert(camera->Core.Transform.orientation);

   return MTRX_MULT(eng_math_mtrx_from_position(position), eng_math_mtrx_from_quat(orientation));
}

static Mtrx
_model_mtrx(Polygon_Broadphase_Entity_State *model)
{
   Mtrx position, orientation;

   position    = eng_math_mtrx_from_position(model->Core.Transform.position);
   orientation = eng_math_mtrx_from_quat(model->Core.Transform.orientation);
   //scale       = eng_math_mtrx_from_scale(model->Polygon.Model.scale);

   return MTRX_MULT(orientation, position);
}

static void
_mtrx_to_glsl(float output[], Mtrx input)
{
   GLfloat invbasis = (GLfloat)1 / BASIS;

   output[0]  = input.r0c0 * invbasis;  output[4]  = input.r1c0 * invbasis;
   output[1]  = input.r0c1 * invbasis;  output[5]  = input.r1c1 * invbasis;
   output[2]  = input.r0c2 * invbasis;  output[6]  = input.r1c2 * invbasis;
   output[3]  = input.r0c3 * invbasis;  output[7]  = input.r1c3 * invbasis;

   output[8]  = input.r2c0 * invbasis;  output[12] = input.r3c0 * invbasis;
   output[9]  = input.r2c1 * invbasis;  output[13] = input.r3c1 * invbasis;
   output[10] = input.r2c2 * invbasis;  output[14] = input.r3c2 * invbasis;
   output[11] = input.r2c3 * invbasis;  output[15] = input.r3c3 * invbasis;
}

static GLuint
load_shader(Eo *obj, GLuint program, GLenum type, const char *path)
{
   GLuint       shader;
   GLint        compiled;
   GLint        info_len;
   FILE        *file;
   uint64_t     size;
   Evas_GL_API *api = elm_glview_gl_api_get(obj);

   // Create a shader object
   shader = api->glCreateShader(type);
   if(!shader) { printf("Cannot create Shader object.\n"); return 0; }

   // Load source code from the specified shader file.
   file = fopen(path, "r");
   if(!file) { printf("Cannot find Shader file.\n"); return 0; }
   fseek(file, 0, SEEK_END);
   size = ftell(file);
   fseek(file, 0, SEEK_SET);
   GLchar buffer[size + 1];
   buffer[size] = '\0';
   fread(buffer, 1, size, file);
   fclose(file);
   const GLchar *source = buffer;
   api->glShaderSource(shader, 1, &source, NULL);

   // Compile the loaded shader source.
   api->glCompileShader(shader);

   // Check to see if compilation is successful.
   api->glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
   if(compiled) { api->glAttachShader(program, shader); return shader; }
   else
   {
      api->glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_len);
      if(info_len > 1)
      {
         char* info_log = malloc(sizeof(char) * info_len);

         api->glGetShaderInfoLog(shader, info_len, NULL, info_log);
         printf("Error compiling shader:\n%s\n======\n%s\n======\n", info_log, buffer);
         free(info_log);
      }
      api->glDeleteShader(shader);
      return 0;
   }
}

// Callbacks
static void
_polygon_render_init_gl(Evas_Object *obj)
{
   Evas_GL_API           *api = elm_glview_gl_api_get(obj);
   Polygon_Render_Buffer *prb = efl_key_data_get(obj, "Polygon_Render_Buffer");

   if (!prb) { printf("Unable to get the Polygon Render Buffer.\n"); return; }

   // Create the program object
   prb->program = api->glCreateProgram();
   if (!prb->program) { printf("Unable to create the Polygon Program Pipeline"); return; }

   // Load the vertex/fragment shaders
   prb->vertex_shader = load_shader(obj, prb->program, GL_VERTEX_SHADER,
                                       "../data/Polygon/Render/Shaders/polygon.vert");
   prb->color_shader  = load_shader(obj, prb->program, GL_FRAGMENT_SHADER,
                                       "../data/Polygon/Render/Shaders/polygon.frag");

   api->glLinkProgram(prb->program);
   api->glUseProgram(prb->program);

   prb->mvp_gpuid    = api->glGetUniformLocation(prb->program, "u_mvp");
   prb->vertex_gpuid = api->glGetAttribLocation(prb->program,  "i_vertex");
   prb->color_gpuid  = api->glGetAttribLocation(prb->program,  "i_color");

   GLint linked;
   api->glGetProgramiv(prb->program, GL_LINK_STATUS, &linked);
   if (!linked)
   {
      GLint info_len = 0;
      api->glGetProgramiv(prb->program, GL_INFO_LOG_LENGTH, &info_len);
      if (info_len > 1)
      {
         char* info_log = malloc(sizeof(char) * info_len);

         api->glGetProgramInfoLog(prb->program, info_len, NULL, info_log);
         printf("Error linking Polygon Program Pipeline:\n%s\n", info_log);
         free(info_log);
      }
      api->glDeleteProgram(prb->program);

      return;
   }

   // Configure the depth buffer.
   api->glEnable(GL_DEPTH_TEST);
   api->glDepthFunc(GL_LESS);

   // Set up our glsl buffer locations.
   api->glGenBuffers(1, &prb->vertex_buffer);
   api->glGenBuffers(1, &prb->element_buffer);
   api->glGenBuffers(1, &prb->color_buffer);

   // Clear our buffer binding.
   api->glBindBuffer(GL_ARRAY_BUFFER, 0);
}

static void
_polygon_render_del_gl(Evas_Object *obj)
{
   printf("_polygon_render_del_gl Checkpoint.\n");

   Evas_GL_API           *api = elm_glview_gl_api_get(obj);
   Polygon_Render_Buffer *prb = efl_key_data_get(obj, "Polygon_Render_Buffer");
   if (!prb) { printf("Unable to get the Polygon Render Buffer.\n"); return; }

   api->glDeleteShader(prb->vertex_shader);
   api->glDeleteShader(prb->color_shader);
   api->glDeleteProgram(prb->program);

   //efl_key_data_del(obj, "Polygon_Render_Buffer");
}

static void
_polygon_render_resize_gl(Evas_Object *obj)
{
   printf("_polygon_render_resize_gl Checkpoint.\n");

   int width, height;

   Evas_GL_API *api = elm_glview_gl_api_get(obj);

   elm_glview_size_get(obj, &width, &height);
   api->glViewport(0, 0, width, height);
}

/*
void apply_material(const struct aiMaterial *mtl)
{
	float c[4];

	GLenum fill_mode;
	int ret1, ret2;
	struct aiColor4D diffuse;
	struct aiColor4D specular;
	struct aiColor4D ambient;
	struct aiColor4D emission;
	ai_real shininess, strength;
	int two_sided;
	int wireframe;
	unsigned int max;

	set_float4(c, 0.8f, 0.8f, 0.8f, 1.0f);
	if(AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_DIFFUSE, &diffuse))
		color4_to_float4(&diffuse, c);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c);

	set_float4(c, 0.0f, 0.0f, 0.0f, 1.0f);
	if(AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_SPECULAR, &specular))
		color4_to_float4(&specular, c);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, c);

	set_float4(c, 0.2f, 0.2f, 0.2f, 1.0f);
	if(AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_AMBIENT, &ambient))
		color4_to_float4(&ambient, c);
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, c);

	set_float4(c, 0.0f, 0.0f, 0.0f, 1.0f);
	if(AI_SUCCESS == aiGetMaterialColor(mtl, AI_MATKEY_COLOR_EMISSIVE, &emission))
		color4_to_float4(&emission, c);
	glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, c);

	max = 1;
	ret1 = aiGetMaterialFloatArray(mtl, AI_MATKEY_SHININESS, &shininess, &max);
	if(ret1 == AI_SUCCESS) {
    	max = 1;
    	ret2 = aiGetMaterialFloatArray(mtl, AI_MATKEY_SHININESS_STRENGTH, &strength, &max);
		if(ret2 == AI_SUCCESS)
			glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shininess * strength);
        else
        	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
    }
	else {
		glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 0.0f);
		set_float4(c, 0.0f, 0.0f, 0.0f, 0.0f);
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, c);
	}

	max = 1;
	if(AI_SUCCESS == aiGetMaterialIntegerArray(mtl, AI_MATKEY_ENABLE_WIREFRAME, &wireframe, &max))
		fill_mode = wireframe ? GL_LINE : GL_FILL;
	else
		fill_mode = GL_FILL;
	glPolygonMode(GL_FRONT_AND_BACK, fill_mode);

	max = 1;
	if((AI_SUCCESS == aiGetMaterialIntegerArray(mtl, AI_MATKEY_TWOSIDED, &two_sided, &max)) && two_sided)
		glDisable(GL_CULL_FACE);
	else
		glEnable(GL_CULL_FACE);
}

void
_polygon_render_mesh_node(Evas_Object *obj, const struct aiScene *scene, const struct aiNode* node)
{
   Evas_GL_API *api = elm_glview_gl_api_get(obj);

   unsigned int i;
   unsigned int n = 0, t;
   struct aiMatrix4x4 m = node->mTransformation;

   // update transform
   aiTransposeMatrix4(&m);
   api->glPushMatrix(); // Deprecated
   api->glMultMatrixf((float*)&m);

   // draw all meshes assigned to this node
   for (; n < node->mNumMeshes; ++n)
   {
      const struct aiMesh* mesh = scene->mMeshes[node->mMeshes[n]];

      apply_material(scene->mMaterials[mesh->mMaterialIndex]);

      if(mesh->mNormals == NULL)
      {
         api->glDisable(GL_LIGHTING);
      } else {
	 api->glEnable(GL_LIGHTING);
      }

      for (t = 0; t < mesh->mNumFaces; ++t)
      {
         const struct aiFace* face = &mesh->mFaces[t];
         GLenum face_mode;

         switch(face->mNumIndices)
         {
            case 1:  face_mode = GL_POINTS;    break;
            case 2:  face_mode = GL_LINES;     break;
            case 3:  face_mode = GL_TRIANGLES; break;
            default: face_mode = GL_POLYGON;   break;
	 }

	 api->glBegin(face_mode);

	 for(i = 0; i < face->mNumIndices; i++)
	 {
	    int index = face->mIndices[i];
	    if(mesh->mColors[0] != NULL)
	       api->glColor4fv((GLfloat*)&mesh->mColors[0][index]);
	    if(mesh->mNormals != NULL)
	       api->glNormal3fv(&mesh->mNormals[index].x);
	    api->glVertex3fv(&mesh->mVertices[index].x);
	 }

	 api->glEnd();
      }
   }

   // draw all children
   for (n = 0; n < node->mNumChildren; ++n)
      _polygon_render_mesh_node(scene, node->mChildren[n])

   api->glPopMatrix(); // Deprecated
}
*/

static void
_polygon_render_mesh(Evas_GL_API *api, Polygon_Render_Buffer *prb,
        Mtrx display, Mtrx orientation, Eng_Mesh *mesh)
{
   float mvp_glsl[16];
   int   size;

   orientation = MTRX_MULT(mesh->orientation, orientation);

   _mtrx_to_glsl(mvp_glsl, MTRX_MULT(orientation, display));
   api->glUniformMatrix4fv(prb->mvp_gpuid, 1, GL_FALSE, mvp_glsl);

   size = mesh->vertices->member_size * eina_inarray_count(mesh->vertices);
   api->glBindBuffer(GL_ARRAY_BUFFER, prb->vertex_buffer);
   api->glBufferData(GL_ARRAY_BUFFER, size, mesh->vertices->members,
                        GL_STATIC_DRAW);
   api->glVertexAttribPointer(prb->vertex_gpuid, 3, GL_FLOAT, GL_FALSE, 0, 0);
   api->glEnableVertexAttribArray(prb->vertex_gpuid);

   size = mesh->elements->member_size * eina_inarray_count(mesh->elements);
   api->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, prb->element_buffer);
   api->glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, mesh->elements->members,
                        GL_STATIC_DRAW);

   size = mesh->textures->member_size * eina_inarray_count(mesh->textures);
   api->glBindBuffer(GL_ARRAY_BUFFER, prb->color_buffer);
   api->glBufferData(GL_ARRAY_BUFFER, size, mesh->textures->members,
                        GL_STATIC_DRAW);
   api->glVertexAttribPointer(prb->color_gpuid, 4, GL_FLOAT, GL_FALSE, 0, 0);
   api->glEnableVertexAttribArray(prb->color_gpuid);

   // This may go outside of this func in the parent altogether...
   size = eina_inarray_count(mesh->elements);
   api->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, prb->element_buffer);
   api->glDrawElements(GL_TRIANGLES, size, GL_UNSIGNED_INT, 0);

   if(mesh->children)
   {
      Eng_Mesh *child;
      EINA_INARRAY_FOREACH(mesh->children, child)
         _polygon_render_mesh(api, prb, display, orientation, child);
   }
}

static void
_polygon_render_draw_gl(Evas_Object *obj)
{
   printf("_polygon_render_draw_gl Checkpoint.\n");

   Mtrx (*_projection_mtrx)(Sclr width, Sclr height, Sclr fov, Sclr near, Sclr far);
   Mtrx display_mtrx, projection_mtrx, view_mtrx, model_mtrx;

   Eng_Mesh *mesh;
   int       width, height;

   Evas_GL_API           *api = elm_glview_gl_api_get(obj);
   Polygon_Render_Buffer *prb = efl_key_data_get(obj, "Polygon_Render_Buffer");
   if (!prb) { printf("ERROR: Unable to get the Polygon Render Buffer.\n"); return; }

   if(!prb->system) return;

   api->glClearDepthf(1.0);
   api->glEnable(GL_CULL_FACE);
   api->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   api->glUseProgram(prb->program);

   elm_glview_size_get(obj, &width, &height);
   if(!strcmp((const char *)prb->camera.Core.Camera.projection, "perspective"))
      _projection_mtrx = _projection_mtrx_perspective;
   if(!strcmp((const char *)prb->camera.Core.Camera.projection, "orthographic"))
      _projection_mtrx = _projection_mtrx_orthographic;
   if(!_projection_mtrx) { printf("ERROR: No valid projection specified.\n"); return; }
   projection_mtrx = _projection_mtrx(sclr(width), sclr(height),
                                      sclr(prb->camera.Core.Camera.field_of_view),
                                      sclr(0.1), sclr(100.0));

   view_mtrx    = _view_mtrx(&prb->camera);
   display_mtrx = MTRX_MULT(view_mtrx, projection_mtrx);

   Polygon_Broadphase_Entity_State *target;
   EINA_INARRAY_FOREACH((Eina_Inarray*)prb->system->prereq.Polygon.Broadphase->graph, target)
   {
      model_mtrx = _model_mtrx(target);
      mesh       = (Eng_Mesh*)target->Polygon.Model.mesh;

      _polygon_render_mesh(api, prb, display_mtrx, model_mtrx, mesh);
   }

   // Optional - Flush the GL pipeline
   api->glFinish();
}

/*


static Eina_Bool
_anim(void *data)
{
   // If the game loop has updated the appropriate camera when this animator goes off...
   elm_glview_changed_set(data);
   return ECORE_CALLBACK_RENEW;
}

static void
_close_cb(void *data, Evas_Object *obj EINA_UNUSED,
          void *event_info EINA_UNUSED)
{
   evas_object_del(data);
}

static void
_gl_del_cb(void *data, Evas *evas EINA_UNUSED, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
   ecore_animator_del(data);
}
*/

