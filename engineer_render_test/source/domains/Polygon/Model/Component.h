#ifndef _POLYGON_MODEL_COMPONENT_H_
#define _POLYGON_MODEL_COMPONENT_H_

#include <eng_component.h>

STATE(
   FIELD(Vec3, bounds)
   FIELD(Vec3, scale)

   FIELD(Pntr, vertices)
   FIELD(Pntr, normals)
   FIELD(Pntr, uv_coords)
   FIELD(Pntr, faces)
   
   FIELD(Pntr, materials)

   FIELD(Pntr, mesh)
)

#endif

