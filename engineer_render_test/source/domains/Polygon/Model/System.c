#include "System.h"

static void
system_start(System_State *system)
{
   printf("System Start Checkpoint. System: Polygon.Model.\n");
}

static void
system_update(System_State *system)
{
}

static void
system_stop(System_State *system)
{
}

static void
entity_start(Entity_State *entity)
{
}

static void
entity_update(Entity_State *entity)
{
}

static void
entity_stop(Entity_State *entity)
{
}


