#ifndef _POLYGON_MODEL_SYSTEM_H_
#define _POLYGON_MODEL_SYSTEM_H_

#include <eng_system.h>

SYMBOL(Polygon, Model)

PREREQS(
)

GLOBAL(
   FIELD(Pntr, meshes)
   FIELD(Pntr, materials)

   EVENTS(
   )
)

ENTITY(
   INPUTS(
   )
   OUTPUTS(
   )
   EVENTS(
   )
)

#endif
