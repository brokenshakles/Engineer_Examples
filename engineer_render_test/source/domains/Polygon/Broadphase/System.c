#include "System.h"

static int
_target_compare(const void *a, const void *b);

static void
system_start(System_State *system)
{
   system->graph = (Pntr)eina_inarray_new(sizeof(Entity_State), 0);
   //system->graph = engineer_math_aabbtree_new(0);

   printf("System Start Checkpoint. System: Polygon.Broadphase. Population: %d\n",
      eina_inarray_count((Eina_Inarray*)system->graph));
}

static void
system_update(System_State *system)
{
}

static void
system_stop(System_State *system)
{
   eina_inarray_free((Eina_Inarray*)system->graph);

   //engineer_math_aabbtree_free(system->graph);
}

static Eina_Bool
system_event__broadphase_insert(System_State *system, const void *notice)
{
   const Entity_State *entity = notice;

   eina_inarray_insert_sorted((Eina_Inarray*)system->graph, entity, _target_compare);

   //engineer_math_aabbtree_insert(system->graph, target->id, target->bounds);

   return EINA_TRUE;
}

static Eina_Bool
system_event__broadphase_update(System_State *system, const void *notice)
{
   const Entity_State *entity = notice;
   Indx                index;

   index = eina_inarray_search_sorted((Eina_Inarray*)system->graph, entity, _target_compare);
   eina_inarray_replace_at((Eina_Inarray*)system->graph, index, entity);

   //engineer_math_aabbtree_update(system->graph, target->id, target->bounds);

   return EINA_TRUE;
}
/*
static bool
system_event__broadphase_remove(System_State *system, void *payload)
{
   Broadphase_Target *target;

   eina_inarray_search();

   engineer_math_aabbtree_remove(system->graph, target->id, target->bounds);
}
*/
void
entity_start(Entity_State *entity)
{
   system_notify(system->entity, "broadphase_insert", entity);

   printf("Entity Start Checkpoint. EntityID: %ld, Symbol: Polygon.Broadphase.System.\n", this);
}

void
entity_update(Entity_State *entity)
{
   system_notify(system->entity, "broadphase_update", entity);
}

void
entity_stop(Entity_State *entity)
{
}

static int
_target_compare(const void *a, const void *b)
{
   const Entity_State *sa = a;
   const Entity_State *sb = b;

   if (sa->id > sb->id) return  1;
   if (sa->id < sb->id) return -1;
   return 0;
}
