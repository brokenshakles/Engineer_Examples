#ifndef _POLYGON_BROADPHASE_SYSTEM_H_
#define _POLYGON_BROADPHASE_SYSTEM_H_

#include <eng_system.h>

SYMBOL(Polygon, Broadphase)

PREREQS(
)

GLOBAL(
   STATE(
      FIELD(Pntr, graph)
   )
   EVENTS(
      EVENT(broadphase_insert, sizeof(Entity_State))
      EVENT(broadphase_update, sizeof(Entity_State))
   )
)

ENTITY(
   INPUTS(
      ASSET(Core,
         COMPONENT(Transform,
            FIELD(Vec3, position)
            FIELD(Quat, orientation)
         )
      )
      ASSET(Polygon,
         COMPONENT(Model,
            FIELD(Vec3, bounds)
            FIELD(Vec3, scale)

            FIELD(Pntr, mesh)
         )
      )
   )
   OUTPUTS(
   )
   EVENTS(
   )
)

#endif
