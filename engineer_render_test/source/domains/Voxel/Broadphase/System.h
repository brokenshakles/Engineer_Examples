#ifndef _VOXEL_BROADPHASE_SYSTEM_H_
#define _VOXEL_BROADPHASE_SYSTEM_H_

#include <eng_system.h>

SYMBOL(Voxel, Broadphase)

PREREQS(
)

GLOBAL(
   STATE(
      FIELD(Pntr, graph)
   )
   EVENTS(
      EVENT(broadphase_insert, sizeof(Entity_State))
      EVENT(broadphase_update, sizeof(Entity_State))
   )
)

ENTITY(
   INPUTS(
      ASSET(Core,
         COMPONENT(Transform,
            FIELD(Vec3, position)
            FIELD(Quat, orientation)
         )
      )
      ASSET(Voxel,
         COMPONENT(Model,
            FIELD(Vec3,  bounds)
            FIELD(Sclr,  shape)
            FIELD(Color, color)
         )
      )
   )
   OUTPUTS(
   )
   EVENTS(
   )
)

#endif
