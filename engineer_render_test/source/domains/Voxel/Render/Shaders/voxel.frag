#version 310 es

#ifdef GL_ES
precision highp float;
#endif

#define MISS   Collision(-1, -1.0, vec3(0.0, 0.0, 0.0))
#define ORIGIN vec3(0.0, 0.0, 0.0);

struct Collider
{
   mat4 matrix;

   vec3 color;
   vec3 size;
   uint type;
};

layout (std140) uniform Model
{
   mat4 opposite;
   mat4 transform;

   vec3 color;
   vec3 size;
   uint type;
};

struct Collision
{
   int   subject;
   float distance;
   vec3  deflection;
};

struct Ray
{
   vec3 inverse;
   vec3 signs;
};

layout (std430, binding = 1) buffer shader_data
{
   Collider objects[];
};

uniform vec2  resolution;
uniform uint  count;
uniform mat4  projection;
uniform vec3  light_direction;

out vec4 FragColor;

mat4
vec3_to_translation_mat4(vec3 v)
{
    return mat4(1.0, 0.0, 0.0, 0.0,
                0.0, 1.0, 0.0, 0.0,
		0.0, 0.0, 1.0, 0.0,
		v.x, v.y, v.z, 1.0);
}

mat4
quat_to_rotation_mat4(vec4 quat)
{
   vec4 sqr;
   vec3 dbl, xxy, www;

   dbl = quat.xyw * vec3(2.0);
   sqr = quat     * quat;
   xxy = dbl.xxy  * quat.yzz;
   www = dbl.zzz  * quat.xyz;

   return mat4(sqr.w + sqr.x - sqr.y - sqr.z, xxy.x + www.z, xxy.y - www.y, 0.0,
               xxy.x - www.z, sqr.w - sqr.x + sqr.y - sqr.z, xxy.z + www.x, 0.0,
               xxy.y + www.y, xxy.z - www.x, sqr.w - sqr.x - sqr.y + sqr.z, 0.0,
                                                             0.0, 0.0, 0.0, 1.0);
}

Collision
iSphere(in vec3 ray_origin, in vec3 ray_direction, in Collider sphere)
{
   vec3 location = sphere.matrix[3].xyz;
   vec3 oc = ray_origin - location;
   float b = 2.0 * dot(oc, ray_direction);
   float c = dot(oc, oc) - sphere.size.x * sphere.size.x;
   float h = b * b - 4.0 * c;

   if(h < 0.0) return MISS;

   float distance = (-b -sqrt(h)) / 2.0;
   vec3  normal   = ((ray_origin + distance * ray_direction) - location) / sphere.size.x;

   return Collision(-1, distance, normal);
}

Collision
iBox(in vec3 ray_origin, in vec3 ray_direction, in Collider box)
{
   mat4 transform = box.matrix;
   mat4 opposite  = inverse(transform);

   // Convert the input ray from cameraspace to model space
   vec3 modelspace_ray_direction = (opposite * vec4(ray_direction, 0.0)).xyz;
   vec3 modelspace_ray_origin    = (opposite * vec4(ray_origin,    1.0)).xyz;

   // Ray-box intersection in model space
   vec3 i = 1.0 / modelspace_ray_direction;
   vec3 n =   i * modelspace_ray_origin;
   vec3 k = abs(i) * box.size;

   vec3 upper = -n - k; // Upper corner of our box.
   vec3 lower = -n + k; // Lower corner of our box.

   float near = max(upper.x, max(upper.y, upper.z));
   float far  = min(lower.x, min(lower.y, lower.z));

   // If the input ray does not intersect the box, return a MISS.
   if(far < near || far < 0.0) return MISS;

   // This is broken for the case of a box surface viewed from a perfect head-on(ortho) angle.
   vec3 normal = -sign(modelspace_ray_direction)
               *  step(upper.yzx, upper.xyz)
               *  step(upper.zxy, upper.xyz);

   // Convert collision normal from model space to camera space
   normal = (transform * vec4(normal, 0.0)).xyz;

   return Collision(-1, near, normal);
}

Collision
iPlane(in vec3 ray_origin, in vec3 ray_direction, in Collider plane)
{
   // Normal of an up facing y plane: 0.0, 1.0, 0.0
   // Equation of a y-aligned plane: y = 0 = ray_origin.y + t * ray_direction.y
   // Normal of a camera facing z plane: 0.0, 0.0, 1.0;
   //
   vec3 location = plane.matrix[3].xyz;

   return Collision(-1, location.y/ray_direction.y, vec3(0.0, 1.0, 0.0));
}

Collision
intersect(in vec3 ray_origin, in vec3 ray_direction)
{
   Collision tests[4];
   int       type;
   int       counter;
   bool      selector;
   int       nearest;
   float     draw_distance;
   float     interval;

   // Set up our maximum draw distance.
   draw_distance = pow(2.0, 31.0);
   interval = draw_distance;

   // For each of our objects, run an intersection test.
   for(counter = 0; counter < int(count); counter++)
   {
      switch(objects[counter].type)
      {
         case uint(1): // Sphere Collider.
            tests[counter] = iSphere(ray_origin, ray_direction, objects[counter]);
         break;

         case uint(2): // Box Collider.
            tests[counter] = iBox(ray_origin, ray_direction, objects[counter]);
         break;

         case uint(3): // Plane Collider.
            tests[counter] = iPlane(ray_origin, ray_direction, objects[counter]);
         break;

         //case 4: // ***Incoming*** Mass/SVO collider.
         //break;
      }
   }

   // Since we are presuming everything is 100% opaque, find the nearest collision, if any.
   for(counter = 0; counter < int(count); counter++)
   {
      selector = tests[counter].distance > 0.0;
      selector = selector && (tests[counter].distance < interval);
      nearest  = selector ? int(counter) : nearest;
      interval = selector ? tests[counter].distance : interval;
   }

   if(interval == draw_distance) return MISS;

   tests[nearest].subject = nearest;
   return tests[nearest];
}

void
main(void)
{
   // uv are the pixel co-ordinates from 0 to 1.
   vec2 uv = (gl_FragCoord.xy/resolution.xy);

   // Determine the aspect ratio of our viewport.
   vec2 aspect_ratio;
   if(resolution.x > resolution.y)
     aspect_ratio = vec2(resolution.x/resolution.y, 1.0);
   else
     aspect_ratio = vec2(1.0, resolution.y/resolution.x);

   // We generate a ray with ray_origin and ray_direction.
   vec3 ray_origin    = ORIGIN;
   vec3 ray_direction = normalize(vec3((-1.0+2.0*uv) * aspect_ratio, -1.0));

   // We intersect the ray with the 3d scene.
   Collision collision = intersect(ray_origin, ray_direction);

   // We draw grey by default.
   vec3 pixel = vec3(0.66666, 0.66666, 0.66666);

   if(collision.subject > -1)
   {
      // Apply our object color to the pixel.
      pixel = objects[collision.subject].color;

      // Apply some simple lighting to our object surfaces.
      pixel *= dot(collision.deflection, light_direction);
   }

   FragColor = vec4(pixel, 1.0);
}

