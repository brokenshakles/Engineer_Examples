#ifndef _PHYSICS_COLLIDER_COMPONENT_H_
#define _PHYSICS_COLLIDER_COMPONENT_H_

#include <eng_component.h>

STATE(
   FIELD(Vec3, bounds)
   FIELD(Sclr, shape)

   FIELD(AABB, bounding_box)

   FIELD(Sclr, mass)
   FIELD(Mtrx, angular_mass)
   FIELD(Vec3, momentum)
   FIELD(Vec3, angular_momentum)
)

#endif

