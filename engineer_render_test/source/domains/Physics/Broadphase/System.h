#ifndef _CORE_BROADPHASE_SYSTEM_H_
#define _CORE_BROADPHASE_SYSTEM_H_

#include <eng_system.h>
#include "Core/Input/System.h"

SYMBOL(Core, Broadphase)

PREREQS(
   ASSET(Core,
      SYSTEM(Input)
   )
)

SYSTEM(
   STATE(
      FIELD(Pntr, graph)
   )
   EVENTS(
      EVENT(broadphase_insert, sizeof(Entity_State))
      EVENT(broadphase_update, sizeof(Entity_State))
   )
)

ENTITY(
   IGNORE()
   TARGET(Core.Transform && Core.Collider)
   INPUTS(
      ASSET(Core,
         COMPONENT(Transform,
            FIELD(Vec3, position)
            FIELD(Quat, orientation)
         )
      )
      ASSET(Core,
         COMPONENT(Collider,
            FIELD(Vec3, bounds)
         )
      )
   )
   OUTPUTS(
   )
   EVENTS(
   )
)

#endif
